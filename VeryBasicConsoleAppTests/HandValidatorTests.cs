using Microsoft.VisualStudio.TestTools.UnitTesting;
using VeryBasicConsoleApplication;
using FluentValidation;
using System.Collections.Generic;
using System.Linq;

namespace VeryBasicConsoleAppTests
{
    [TestClass]
    public class HandValidatorTests : AbstractValidator<Hand>
    {
        Hand hand1;
        Hand hand2;

        [TestInitialize]
        public void TestInitialize()
        {
            hand1 = new Hand();
            hand2 = new Hand();

            hand1.Draw(new Card(CardValue.Ace, CardSuit.Clubs));
            hand1.Draw(new Card(CardValue.Ace, CardSuit.Hearts));
            hand1.Draw(new Card(CardValue.Ace, CardSuit.Diamonds));
            hand1.Draw(new Card(CardValue.Ace, CardSuit.Spades));
            hand1.Draw(new Card(CardValue.Five, CardSuit.Clubs));

            hand1.HandRank = FiveCardPokerScorer.GetHandRank(hand1.Cards).ToString();


            hand2.Draw(new Card(CardValue.Ace, CardSuit.Clubs));
            hand2.Draw(new Card(CardValue.Ace, CardSuit.Hearts));
            hand2.Draw(new Card(CardValue.King, CardSuit.Diamonds));
            hand2.Draw(new Card(CardValue.Nine, CardSuit.Spades));
            hand2.Draw(new Card(CardValue.Five, CardSuit.Clubs));

            hand2.HandRank = FiveCardPokerScorer.GetHandRank(hand2.Cards).ToString();

        }

        [TestMethod]
        public void RuleFor_CardCountEqualToFive_ReturnsTrue()
        {
            var validator = new HandValidator();

            Assert.IsTrue(new List<Hand> { hand1, hand2 }.All(h => validator.Validate(h).IsValid));
        }

        [TestMethod]
        public void RuleFor_CardCountNotEqualToFive_ReturnsFalse()
        {
            hand1.Draw(new Card(CardValue.Seven, CardSuit.Clubs));
            hand2.Draw(new Card(CardValue.Eight, CardSuit.Clubs));

            var validator = new HandValidator();

            Assert.IsFalse(new List<Hand> { hand1, hand2 }.All(h => validator.Validate(h).IsValid));
        }
    }
}
