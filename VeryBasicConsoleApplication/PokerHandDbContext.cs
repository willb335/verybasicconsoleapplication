﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace VeryBasicConsoleApplication
{
   public class PokerHandDbContext : DbContext
    {

        public PokerHandDbContext() : base("name=HandContext")
        {
            Database.SetInitializer<PokerHandDbContext>(new DropCreateDatabaseAlways<PokerHandDbContext>());
        }

        public DbSet<Hand> Hands { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Admin");

            //Map entity to table
            modelBuilder.Entity<Hand>().Map(m =>
            {
                m.Properties(p => new { p.HandRank });
                m.ToTable("HandInfo");
            }).Map(m => {
                m.Properties(p => new { p.Message });
                m.ToTable("DummyTable");
            });

            modelBuilder.Entity<Card>().ToTable("CardInfo");

        }






    }


}
