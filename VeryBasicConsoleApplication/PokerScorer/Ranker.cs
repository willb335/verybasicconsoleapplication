﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeryBasicConsoleApplication
{
    class Ranker
    {
        public Ranker(Func<IEnumerable<Card>, bool> eval, HandRank rank)
        {
            Eval = eval;
            Rank = rank;
        }

        public Func<IEnumerable<Card>, bool> Eval { get; }

        public HandRank Rank { get; }

    }
}
