﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeryBasicConsoleApplication
{
    public class Hand
    {
        public Hand()
        {
            Cards = new List<Card>();
        }

        public Hand(string message)
        {
            Cards = new List<Card>();
            Message = message;

        }

        public int HandId { get; set; }

        public List<Card> Cards { get; set; }
        public string Message { get; set; }
        public string HandRank { get; set; }
        

        public void Draw(Card card) => Cards.Add(card);
    }
}
