﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeryBasicConsoleApplication
{
    public class Card
    {
        public Card()
        {

        }

        public Card(CardValue value, CardSuit suit)
        {
            Value = value;
            Suit = suit;
        }
        public int CardId { get; set; }
        public CardValue Value { get; set; }
        public CardSuit Suit { get; set; }


        public override string ToString() => $"{Value} of {Suit}";
    }
}
