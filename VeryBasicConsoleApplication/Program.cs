using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeryBasicConsoleApplication
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Saving data to database...");
            PerformDatabaseOperations();

            Console.WriteLine($"   [{GetHandScoreFromDatabase()}]");
            Console.ReadLine();


        }

        
        public static void PerformDatabaseOperations()
        {
            Hand hand1 = new Hand("Dummy data");

            hand1.Draw(new Card(CardValue.Ace, CardSuit.Clubs));
            hand1.Draw(new Card(CardValue.Ace, CardSuit.Hearts));
            hand1.Draw(new Card(CardValue.Ace, CardSuit.Diamonds));
            hand1.Draw(new Card(CardValue.Ace, CardSuit.Spades));
            hand1.Draw(new Card(CardValue.Five, CardSuit.Clubs));

            hand1.HandRank = FiveCardPokerScorer.GetHandRank(hand1.Cards).ToString();

            Hand hand2 = new Hand("More data...");

            hand2.Draw(new Card(CardValue.Ace, CardSuit.Clubs));
            hand2.Draw(new Card(CardValue.Ace, CardSuit.Hearts));
            hand2.Draw(new Card(CardValue.King, CardSuit.Diamonds));
            hand2.Draw(new Card(CardValue.Nine, CardSuit.Spades));
            hand2.Draw(new Card(CardValue.Five, CardSuit.Clubs));

            hand2.HandRank = FiveCardPokerScorer.GetHandRank(hand2.Cards).ToString();

            // validate the hand
            HandValidator validator = new HandValidator();
            bool success = new List<Hand> { hand1, hand2 }.All(h => validator.Validate(h).IsValid);

            if(!success)
            {
                ValidationResult results = validator.Validate(new List<Hand> { hand1, hand2 }.First(h => !validator.Validate(h).IsValid));
                IList<ValidationFailure> failures = results.Errors;

                Console.WriteLine($"Validation failed {failures}");
                Console.WriteLine("Poker hand not saved");
                Console.ReadLine();

                return;
            }



            using (PokerHandDbContext db = new PokerHandDbContext())
            {

                db.Hands.Add(hand1);
                db.Hands.Add(hand2);

                db.SaveChanges();
            }

            Console.Write("Poker hand saved!");


        }

        public static string GetHandScoreFromDatabase()
        {
            using (PokerHandDbContext db = new PokerHandDbContext())
            {

                List<string> hands = db.Hands.Select(h => h.HandRank).ToList();
                string delimiter = ",";

                return hands.Aggregate((i, j) => i + delimiter + j);

            }


        }
    }
}
