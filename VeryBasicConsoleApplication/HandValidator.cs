﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeryBasicConsoleApplication
{
    public class HandValidator : AbstractValidator<Hand>
    {
        public HandValidator()
        {
            RuleFor(h => h.Cards.Count).Equal(5);
          
        }

    }
}
